<?php
/**
 * Plugin Name:     Meu Plugin Joinha
 * Plugin URI:
 * Description:     "With a little bit of imagination, anything is possible." Macgyver, Angus.
 * Author:          Yogh
 * Author URI:      https://www.yogh.com.br/
 * Text Domain:     meu-plugin-joinha
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Meu_Plugin_Joinha
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'not allowed' );
}

new Meu_Plugin_Joinha;

final class Meu_Plugin_Joinha {

	function __construct() {
		add_filter( 'the_title', array( __CLASS__, 'adicionar_data' ), 10, 2 );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_scripts' ) );

		add_action( 'plugins_loaded', array( __CLASS__, 'init' ) );
	}

	static function init() {
		if ( function_exists( 'WC' ) ) {
			include self::get_path() . 'includes/class-meu-plugin-joinha-wc-myaccount.php';
		}
	}

	static function adicionar_data( $title, $post_id ) {

		$data_do_post = get_post_field( 'post_date', $post_id );

		$title .= ' - ' . $data_do_post;

		return $title;
	}

	static function enqueue_scripts() {
		//incluir estilos somente no frontend
		if ( ! is_admin() ) {
			wp_enqueue_style(
				'meu-plugin-joinha-style',
				self::get_url() . 'assets/css/style.css',
				null,
				@filemtime( self::get_url() . 'assets/css/style.css' )
			);
		}
		//incluir scripts somente no backend
		if ( is_admin() ) {
			wp_enqueue_script(
				'meu-plugin-joinha-script',
				self::get_url() . 'assets/js/script.js',
				array( 'jquery', 'jquery-form', 'jquery-ui-datepicker' )
			);
		}
	}

	static function get_path() {
		return plugin_dir_path( __FILE__ );
	}

	static function get_url() {
		return plugin_dir_url( __FILE__ );
	}
}


add_filter( 'the_title', 'meu_plugin_joinha_adicionar_data', 10, 2 );
function meu_plugin_joinha_adicionar_data( $title, $post_id ) {

	$data_do_post = get_post_field( 'post_date', $post_id );

	$title .= ' - ' . $data_do_post;

	return $title;
}
