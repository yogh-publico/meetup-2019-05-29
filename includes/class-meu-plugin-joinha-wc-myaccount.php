<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'not allowed' );
}

new Meu_Plugin_Joinha_WC_MyAccount;

class Meu_Plugin_Joinha_WC_MyAccount {

	function __construct() {
		add_filter( 'woocommerce_account_menu_items', array( __CLASS__, 'remove_menu_logout' ) );
	}

	static function remove_menu_logout( $menu_items ) {
		if ( isset( $menu_items['customer-logout'] ) ) {
			unset( $menu_items['customer-logout'] );
		}

		return $menu_items;
	}
}